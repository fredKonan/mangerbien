package com.codescript.mangerbien.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;
@Component
@Getter
@Setter
@AllArgsConstructor
public class Menu implements Serializable {
   private String nom;
   private String description;
   private Double price;
   private String image_url;

   protected Menu(){}

}
