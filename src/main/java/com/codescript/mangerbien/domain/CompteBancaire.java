package com.codescript.mangerbien.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Getter
@Setter
@AllArgsConstructor
public class CompteBancaire implements Serializable {
    private String nom;
    private  String prenom;
    private String banque;
    private String numero_compte;

    protected CompteBancaire(){}

}
