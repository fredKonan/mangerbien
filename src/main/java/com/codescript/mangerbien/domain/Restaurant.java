package com.codescript.mangerbien.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;


import javax.persistence.*;
import java.util.List;

@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RESTAURANT")
@Getter
@Setter
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "restaurantId")
    private Long id;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "Ville", nullable = false)
    private String ville;

    @Column(name = "quartier", nullable = false)
    private String quartier;

    @Column(name = "commune")
    private String commune;

    @Column(name = "addresse", nullable = false)
    private String addresse;

    @Column(name = "presentationImage_url")
    private String presentationImage_url;

    @Type(type = "json")
    @Column(name = "menu", columnDefinition ="json", nullable = false)
    private List<Menu> menu;

}
