package com.codescript.mangerbien.auth;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurateurRepository extends JpaRepository <Restaurateur, Long> {
   Restaurateur findByUsername(String username);
}
