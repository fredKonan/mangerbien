package com.codescript.mangerbien.auth;

import com.codescript.mangerbien.domain.CompteBancaire;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
@Entity
@Table(name = "RESTAURATEUR")
@Getter
@Setter
public class Restaurateur {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "restaurateurId")
    private Long id;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prenom", nullable = false)
    private String prenom;

    @Column(name = "address")
    private String address;

    @Column(name = "piece_ditentite", nullable = false, updatable = false)
    private String cni;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "restaurant", nullable = false)
    private String nomRestaurant;

    @Column(name = "password", nullable = false)
    private String password;

    @Type(type = "json")
    @Column(name = "compte_bancaire",columnDefinition = "json", nullable = false)
    private CompteBancaire compteBancaire;

}
