package com.codescript.mangerbien.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class RestaurateurDetailsService implements UserDetailsService {

    @Autowired
    private final RestaurateurRepository restaurateurRepository;

    public RestaurateurDetailsService(RestaurateurRepository restaurateurRepository) {
        super();
        this.restaurateurRepository = restaurateurRepository;
    }

    /**
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       Restaurateur restaurateur = this.restaurateurRepository.findByUsername(username);
        if(null == restaurateur){
            throw  new UsernameNotFoundException("Cannot find username "+ username);
        }
        return new RestaurateurPrincipal(restaurateur);
    }
}
