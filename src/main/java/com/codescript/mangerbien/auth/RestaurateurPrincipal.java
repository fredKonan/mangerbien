package com.codescript.mangerbien.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class RestaurateurPrincipal implements UserDetails {
    private final Restaurateur restaurateur;

    public RestaurateurPrincipal(Restaurateur restaurateur) {
        super();
        this.restaurateur = restaurateur;
    }

    /**
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("RESTAURATEUR"));
    }

    /**
     * @return
     */
    @Override
    public String getPassword() {
        return restaurateur.getPassword();
    }

    /**
     * @return
     */
    @Override
    public String getUsername() {
      return  restaurateur.getUsername();
    }

    /**
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * @return
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
