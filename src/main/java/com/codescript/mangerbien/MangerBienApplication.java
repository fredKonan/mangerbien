package com.codescript.mangerbien;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Manger Bien", version = "1.0.0", description = "Manger bien Restaurant Api"))
public class MangerBienApplication {

	public static void main(String[] args) {
		SpringApplication.run(MangerBienApplication.class, args);
	}

}
