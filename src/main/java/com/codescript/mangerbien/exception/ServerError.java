package com.codescript.mangerbien.exception;

public class ServerError extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public ServerError(String message) {
       super(message);
    }
}
