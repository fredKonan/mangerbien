package com.codescript.mangerbien.service;

import com.codescript.mangerbien.domain.Restaurant;
import com.codescript.mangerbien.exception.ResourceNotFoundException;
import com.codescript.mangerbien.exception.ServerError;
import com.codescript.mangerbien.repository.RestaurantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RestaurantService {
    private static final Logger LOG = LoggerFactory.getLogger(RestaurantService.class);

    @Autowired
    private RestaurantRepository restaurantRepository;

    public Map<String, Object> getRestaurantByReference (String ville, String commune, String quartier, String nom, int page, int size){
        try{
            List<Restaurant> restaurants = new ArrayList<Restaurant>();
            Pageable pagination = PageRequest.of(page, size);
            Page<Restaurant> restaurantPage;
            if(commune == null && nom == null){
                restaurantPage= restaurantRepository.findByVilleAndQuartierContaining(ville, quartier,pagination);
            }
            if(commune == null && nom!=null){
                restaurantPage = restaurantRepository.findByVilleAndQuartierAndNomContaining(ville, quartier, nom, pagination);
            } if(commune!=null && nom==null) {
                restaurantPage = restaurantRepository.findByVilleAndCommuneAndQuartierContaining(ville, commune, quartier, pagination);
            }
            else{
                restaurantPage = restaurantRepository.findByVilleAndCommuneAndQuartierAndNomContaining(ville, commune, quartier, nom, pagination);
            }
            restaurants = restaurantPage.getContent();
            Map<String, Object> response =  new HashMap<String, Object>();
            response.put("restaurant", restaurants);
            response.put("totalPages", restaurantPage.getTotalPages());
            return response;
        }catch (Exception e){
            throw new ServerError(e.getMessage());
        }
    }
    public Restaurant addRestaurant(Restaurant restaurant){
        LOG.debug("adding restaurant...");
        if(restaurant==null){
            throw new RuntimeException("We cannot add this restaurant because it doesn't follow all requirements");
        }
        try{
            restaurantRepository.save(restaurant);
            LOG.info("Your restaurant has been successfully added. Welcome to Manger Bien");
        }catch (ServerError e){
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
        return restaurant;
    }

    public boolean removeRestaurant(Long restaurantId){
        Restaurant restaurant = restaurantRepository.findById(restaurantId)
                .orElseThrow(() -> new ResourceNotFoundException
                        ("User Not Found"));
        try{
            LOG.debug("removing restaurant...");
            restaurantRepository.deleteById(restaurantId);
            LOG.info("Restaurant Successfully delete");
        }catch (ServerError e){
            LOG.error(e.getMessage(), Arrays.toString(e.getStackTrace()));
            throw new RuntimeException(e);
        }
         return true;
    }

    public Restaurant updateRestaurant(Long restaurantId, Restaurant restaurant){
        Restaurant currentRestaurant =  restaurantRepository.findById(restaurantId)
                .orElseThrow(() -> new ResourceNotFoundException
                        ("User Not Found"));
        LOG.debug("Updating restaurant...");
        currentRestaurant.setAddresse(restaurant.getAddresse());
        currentRestaurant.setCommune(restaurant.getCommune());
        currentRestaurant.setMenu(restaurant.getMenu());
        currentRestaurant.setNom(restaurant.getNom());
        currentRestaurant.setQuartier(restaurant.getQuartier());
        currentRestaurant.setPresentationImage_url(restaurant.getPresentationImage_url());
        currentRestaurant.setVille(restaurant.getVille());

        LOG.info("restaurant successfully update");
       return restaurantRepository.save(currentRestaurant);

    }
}
