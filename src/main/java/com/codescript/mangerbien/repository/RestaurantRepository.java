package com.codescript.mangerbien.repository;

import com.codescript.mangerbien.domain.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    Page<Restaurant> findByVilleAndCommuneAndQuartierAndNomContaining(String ville, String commune, String quartier, String nom, Pageable pageable);

    Page<Restaurant> findByVilleAndQuartierContaining(String ville, String quartier, Pageable pagination);

    Page<Restaurant> findByVilleAndQuartierAndNomContaining(String ville, String quartier, String nom, Pageable pagination);
    Page<Restaurant> findByVilleAndCommuneAndQuartierContaining(String ville, String commune, String quartier, Pageable pageable);




}
