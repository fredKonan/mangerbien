package com.codescript.mangerbien.controller;

import com.codescript.mangerbien.auth.Restaurateur;
import com.codescript.mangerbien.auth.RestaurateurRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/restaurateur")
public class RestaurateurController {
    private static final Logger LOG = LoggerFactory.getLogger(RestaurateurController.class);
    private final RestaurateurRepository restaurateurRepository;

    @Autowired
    public RestaurateurController(RestaurateurRepository restaurateurRepository) {
        this.restaurateurRepository = restaurateurRepository;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createRestaurateur(@RequestBody Restaurateur restaurateur){
        LOG.debug("create your profile...");
        try{
            restaurateurRepository.save(restaurateur);
            LOG.info("your profile has been created");
        }catch (HttpServerErrorException e){
            LOG.error(e.getMessage(), e.getStatusCode());
            throw new RuntimeException(e);
        }catch (ResourceAccessException | DataAccessResourceFailureException e){
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Restaurateur getRestaurateurByUsername(@RequestParam(value = "username") String username){
      return  restaurateurRepository.findByUsername(username);
    }
}
