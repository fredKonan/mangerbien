package com.codescript.mangerbien.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/error")
public class WebErrorController implements ErrorController {

    @GetMapping
    public String getErrorPage(HttpServletRequest request, Model model){
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if(status !=null){
            int statusCode = Integer.parseInt(status.toString());
            String text = "";
            if(statusCode== HttpStatus.NOT_FOUND.value()){
                text = "La page que vous essayez d'acceder est introuvable";
            }
            else if(statusCode == HttpStatus.UNAUTHORIZED.value()){
                text="Vous etes pas authorizer a acceder a cette page";
            }
            else if(statusCode== HttpStatus.FORBIDDEN.value()){
                text="vous n'avez pas la permittion pour cette page";
            }
            else{
                text ="Erreur inconnu";
            }
            model.addAttribute("errorText", text);
            model.addAttribute("errorCode", statusCode);
        }
        else{
            model.addAttribute("errorText", "Une erreur incconu est survenu");
            model.addAttribute("errorCode", "inconnu");
        }
        return "error";
    }
}
