package com.codescript.mangerbien.controller;

import com.codescript.mangerbien.domain.Restaurant;
import com.codescript.mangerbien.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.Map;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class RestaurantController {

   private  final RestaurantService restaurantService;

   @Autowired
    public RestaurantController( RestaurantService restaurantService) {

        this.restaurantService = restaurantService;
    }

    @GetMapping("/restaurants")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public Map<String, Object> getAllRestaurant(
            @RequestParam(value = "ville", required = true) String ville,
            @RequestParam(value = "commune", required = false) String commune,
            @RequestParam(value = "quartier", required = false) String quartier,
            @RequestParam(value = "nom", required = false) String nom,
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value = "size", defaultValue = "5", required = false) int size
    ){
    return restaurantService.getRestaurantByReference(ville, commune, quartier, nom, page, size);
    }

    @PreAuthorize(value = "hasRole(RESTAURATEUR)")
    @PostMapping("/restaurants")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Restaurant addRestaurant(@RequestBody Restaurant restaurant){
       return restaurantService.addRestaurant(restaurant);
    }

    @PutMapping("/restaurants/{id}")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public Restaurant updateRestaurant(@PathVariable("id") Long id, @RequestBody Restaurant restaurant){
        return restaurantService.updateRestaurant(id, restaurant);
    }

    @DeleteMapping("/restaurants/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public boolean deleteRestaurant(@PathVariable("id") Long id){
       return restaurantService.removeRestaurant(id);
    }

}
